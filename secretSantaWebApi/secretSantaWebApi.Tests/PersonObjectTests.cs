using secretSantaWebApi.Implementations;
using System;
using Xunit;

namespace secretSantaWebApi.Tests
{
    public class PersonObjectTests
    {
        [Fact]
        public void PersonOperationsAllowsInitilizingOfPerson()
        {
            var person = new Person("Test", "Person123");
            Assert.NotNull(person);
        }

        [Fact]
        public void PersonAllowChangePropertiesOutsideOfInitilization()
        {
            var person = new Person("Person", "StartTest");
            person.firstName = "Another";
            person.lastName = "ChangeTest";

            Assert.True(person.firstName != "Person");
            Assert.True(person.lastName != "StartTest");
        }
    }
}
