﻿using secretSantaWebApi.Implementations;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace secretSantaWebApi.Tests
{
   public class GroupObjetcTests
    {
        private Person testPerson { get; set; }

        public GroupObjetcTests()
        {
            this.testPerson = new Person("Test", "Individual");
        }

        [Fact]
        public void EnsureDataIsInitializedWithGroupObject()
        {
            var group = new Group(this.testPerson);

            Assert.NotNull(group);
            Assert.NotNull(group.Primary);
        }

        [Fact]
        public void ValidateAbilityToAddMultiplePersonsToGroup()
        {
            var group = new Group(this.testPerson);
            group.Secondary = new Person("Second", "PersonTest");
            group.AddChild(new Person("Third", "PersonTest"));

            Assert.NotNull(group.Primary);
            Assert.NotNull(group.Secondary);
            Assert.NotEmpty(group.Children);
        }
    }
}
