﻿using secretSantaWebApi.Controllers;
using secretSantaWebApi.Implementations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace secretSantaWebApi.Tests
{
    public class MatchupProcessingTests
    {
        private Group[] participantGroups { get; set; }

        public MatchupProcessingTests()
        {
            PopulateInitTestDataForGroups();
        }

        
        [Fact]
        public void ValidateAlgorithmProducesResults()
        {
            var processorControl = new PeopleController();
            var matchResults = processorControl.GetMatches(this.participantGroups);
            Assert.NotEmpty(matchResults);
        }

        [Fact]
        public void ValidateNoDuplicatesInResults()
        {
            var processorControl = new PeopleController();
            var matchResults = processorControl.GetMatches(this.participantGroups);

            List<Person> resultsItems = matchResults.Select(m => m.MatchPerson1).ToList();
            resultsItems.AddRange(matchResults.Select(m => m.MatchPerson2).ToList());

            var duplicatesFound = resultsItems.Count != resultsItems.Distinct().Count();

            Assert.False(duplicatesFound);
        }

        private void PopulateInitTestDataForGroups()
        {
            var group1 = new Group(new Person("Frank", "Sinatra"));
            group1.Secondary = new Person("Suzanne", "Sinatra");

            var group2 = new Group(new Person("Sammy", "Davis"));
            group2.Secondary = new Person("Cassendra", "Davis");
            group2.AddChild(new Person("Zachary", "Davis"));

            var group3 = new Group(new Person("Eric", "Watson"));

            var group4 = new Group(new Person("Henry", "Vanstein"));
            group4.Secondary = new Person("Beatrice", "Vanstein");
            group4.AddChild(new Person("Edward", "Vanstein"));
            group4.AddChild(new Person("Alice", "Vanstein"));
            
            this.participantGroups = new Group[]{ group1, group2, group3, group4 };
        }
    }
}
