﻿using secretSantaWebApi.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace secretSantaWebApi.Implementations
{
    public class Person : IPerson
    {
        public string firstName { get; set; }
        public string lastName { get; set; }

        public Person(string firstName, string lastName)
        {
            this.firstName = firstName;
            this.lastName = lastName;
        }
    }
}
