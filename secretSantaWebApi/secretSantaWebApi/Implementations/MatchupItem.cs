﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace secretSantaWebApi.Implementations
{
    public class MatchupItem
    {
        public Person MatchPerson1 { get; set; }
        public Person MatchPerson2 { get; set; }
    }
}
