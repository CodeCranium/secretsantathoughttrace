﻿using Newtonsoft.Json;
using secretSantaWebApi.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace secretSantaWebApi.Implementations
{
    public class Group : IGroup
    {
        public int Id { get; set; }

        public Person Primary { get; set; }
        public Person Secondary { get; set; }

        [JsonProperty(PropertyName = "children")]
        public Person[] Children { get; private set; }

        public Group(Person primaryPerson)
        {
            this.Primary = primaryPerson;
        }

        public void AddChild(Person person)
        {
            if (this.Children == null)
            {
                this.Children = new Person[0];
            }

            var newChildList = this.Children.ToList();
            newChildList.Add(person);
            this.Children = newChildList.ToArray();
        }
    }
}
