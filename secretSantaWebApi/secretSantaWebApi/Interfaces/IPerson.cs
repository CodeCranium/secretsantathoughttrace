﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace secretSantaWebApi.Interfaces
{
    public interface IPerson
    {
        string firstName { get; set; }
        string lastName { get; set; }
    }
}
