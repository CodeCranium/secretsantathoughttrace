﻿using secretSantaWebApi.Implementations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace secretSantaWebApi.Interfaces
{
    public interface IGroup
    {
        int Id { get; set; }

        Person Primary { get; set; }
        Person Secondary { get; set; }
        Person[] Children { get; }
    }
}
