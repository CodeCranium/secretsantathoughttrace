﻿using secretSantaWebApi.Implementations;
using secretSantaWebApi.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Group = secretSantaWebApi.Implementations.Group;

namespace secretSantaWebApi.Data
{
    public static class MockDataProvider
    {
        internal static IEnumerable<IGroup> GetMockGroupsParticipating()
        {
            List<Group> groups = new List<Group>();

            var grp1 = new Group(new Person("Michael", "Jordan"))
            {
                Secondary = new Person("Cindy", "Jordan")
            };
            grp1.AddChild(new Person("Mitch", "Jordan"));
            grp1.AddChild(new Person("Michelle", "Jordan"));
            groups.Add(grp1);

            var grp2 = new Group(new Person("Bruce", "Bowen"))
            {
                Secondary = new Person("Suzie", "Bowen")
            };
            grp2.AddChild(new Person("Brad", "Bowen"));
            grp2.AddChild(new Person("Vickie", "Bowen"));
            groups.Add(grp2);

            var grp3 = new Group(new Person("Scotty", "Pippen"))
            {
                Secondary = new Person("Aretha", "Pippen")
            };
            groups.Add(grp3);

            var grp4 = new Group(new Person("Greg", "Papovich"));
            groups.Add(grp4);

            return groups;
        }
    }
}
