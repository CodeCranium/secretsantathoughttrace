﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using secretSantaWebApi.Data;
using secretSantaWebApi.Implementations;
using secretSantaWebApi.Interfaces;

namespace secretSantaWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PeopleController : ControllerBase
    {
        public List<IGroup> Participants { get; set; }
        public PeopleController()
        {
            
        }
        
        // GET: api/People
        [HttpGet]
        public IEnumerable<IGroup> Get()
        {
            var groups = MockDataProvider.GetMockGroupsParticipating();
            this.Participants = groups.ToList();

            return this.Participants;
        }

        // POST: api/People/Match
        [HttpPost("Match")]
        public IEnumerable<MatchupItem> GetMatches(Group[] participants)
        {
            var matchupList = new List<MatchupItem>();

            try
            {
                var kvpListParticipants = (List<KeyValuePair<int, Person>>)GenerateKeyValueList(participants);
                var pulledList = kvpListParticipants.ToList();

                foreach (var participant in kvpListParticipants.Where(x=>x.Value != null))
                {
                    if (pulledList.Any(x => x.Value == participant.Value))
                    {
                        var remainingListLength = pulledList.Count();

                        //TODO: Account for a forever loop due to no additional matches to be met but participants still in the list
                        //TODO: Check for a single non-matchable person (an un-even amount of participants, unable to match up all).
                        
                        while (remainingListLength > 1 && (pulledList.Select(v => v.Key).Distinct().Count() > 1))
                        {
                            var randomListVal = new Random().Next(0, remainingListLength);

                            if (participant.Key != pulledList[randomListVal].Key)
                            {
                                // Match is good, continue with process to match up participants.

                                // Add the match to the MatchItemList
                                matchupList.Add(new MatchupItem()
                                {
                                    MatchPerson1 = participant.Value,
                                    MatchPerson2 = pulledList[randomListVal].Value
                                });
                                // De-queue items from list that match (remove it)
                                pulledList.Remove(pulledList[randomListVal]);
                                pulledList.Remove(participant);
                                
                                // move to the next one.
                                break;
                            } // otherwise pick next random...
                        }

                        if (remainingListLength == 1)
                        {
                            // There is an odd-man-out and no-one to match the odd person up with.
                            matchupList.Add(new MatchupItem() {
                                MatchPerson1 = participant.Value,
                                //MatchPerson2 = new Person("Unmatchable", "[Due to number of individuals, no match for this person]")
                            });
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return matchupList;
        }

        private object GenerateKeyValueList(Group[] participants)
        {
            var keyMatchupList = new List<KeyValuePair<int, Person>>();
            for (int i = 0; i < participants.Length; i++)
            {
                if (participants[i] != null)
                {
                    if (participants[i].Primary != null)
                    {
                        keyMatchupList.Add(new KeyValuePair<int, Person>(i + 1, participants[i].Primary));
                    }
                    if (participants[i].Secondary != null)
                    {
                        keyMatchupList.Add(new KeyValuePair<int, Person>(i + 1, participants[i].Secondary));
                    }
                    if (participants[i].Children != null)
                    {
                        foreach (var child in participants[i].Children)
                        {
                            keyMatchupList.Add(new KeyValuePair<int, Person>(i + 1, child));
                        }
                    }
                }
            }

            return keyMatchupList;
        }


        // GET: api/People/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/People
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

    }
}
