# Secret Santa Project - ThoughtTrace Interview Project - Brandon Fuller

## Running the DotNET Core WebAPI BackEnd  (requires dotNET core v2.0)
- Run the following commands from the `.\secret-santa\secretSantaWebApi\secretSantaWebApi\` project folder
- `dotnet run`
- Navigate to https://localhost:5000/swagger in web browser to view Swagger for the projects API backend

## Unit Tests - to run the unit tests for the project
- From the `.\secret-santa\secretSantaWebApi\secretSantaWebApi.Tests\` directory in command line
- run `dotnet test`


## Running the Angular FrontEnd Application for the Secret Santa Project
- Run the following commands from the `.\secret-santa\secretSantaFrontEnd\` project folder
- Install the Angular CLI (if not already installed on the system): `npm install -g @angular/cli`
- Run `npm install` to install app dependencies
- Run `ng serve -o` to start the server and launch the app


##Deployed to Amazon AWS
`Demo Instance can be run at:  http://52.15.229.11/ for testing/demo purposes`

`__________________________________________________________________________________________________________________________`
`|                                                       Project requirements                                             |`
`--------------------------------------------------------------------------------------------------------------------------`
`|                                                     Secret Santa as a Service                                          |`
`| Secret Santa is a popular holiday tradition for a gift exchange within a group of people. Each person's name is        |`
`|  written on a piece of paper and randomly selected one at a time by every person in the group to determine the         |`
`|  recipient of their Secret Santa gift. However, physically drawing names is not always successful, since it is         |`
`|  possible that a person could draw their own name or the name of their significant other.                              |`
`| Create a simple web application for choosing names for a Secret Santa gift exchange.                                   |`
`|        - This service should be accessible from a user interface via web browser and through a REST API.               |`
`|        - The service should allow for specifying the names of the individuals participating in the exchange and a      |`
`|            mechanism that will allow the user to specify groups of 2 or more people that should be prevented from      |`
`|            selecting each other (i.e.: prevent family members from selecting fellow family members).                   |`
`|        - The result should be a list containing each participant's name and the name of their gift exchange recipient. |`
`|                                                                                                                        |`
`|        - For purposes of this coding submission, you can use any languages/frameworks/libraries of your choice.        |`
`|        - For the sake of time, you do not have to implement any type of authentication or persist data.                |`
`|                                                                                                                        |`
`--------------------------------------------------------------------------------------------------------------------------`