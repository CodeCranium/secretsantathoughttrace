import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SecretSantaFormComponent } from './secretSantaForm/secretSantaForm.component';
import { ControlValueAccessorComponent } from './controlValueAccessor/controlValueAccessor.component';


const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/secretSantaForm' },
  { path: 'secretSantaForm',  component: SecretSantaFormComponent },
  { path: 'controlvalueaccessor', component: ControlValueAccessorComponent },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {
  static components = [ SecretSantaFormComponent, ControlValueAccessorComponent,
  ];
}

