import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpErrorResponse, HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { catchError } from 'rxjs/operators';
import { Group } from '../shared/group';
import { Observable } from 'rxjs';
import { map, filter, switchMap } from 'rxjs/operators';
import { MatchupItem } from '../shared/matchupItem';

const httpOptions = {
  headers: new HttpHeaders({
    'Access-Control-Allow-Origin': '*'
  })
};

@Injectable({
  providedIn: 'root'
})
export class ParticipantService {

  private baseUrl = 'http://localhost:5000';
  private controllerEndpoint = '/api/People/';

  constructor(private http: HttpClient) {}

  getParticipants(): Observable<Group[]> {
    return this.http.get<Group[]>(this.baseUrl + this.controllerEndpoint);
  }

  generateMatchupList(participants: Group[]): Observable<MatchupItem[]> {
    return this.http.post<MatchupItem[]>(this.baseUrl + this.controllerEndpoint + `Match`, participants);
  }

}
