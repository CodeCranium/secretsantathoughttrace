import { Person } from './person';
export class Group {

    constructor(
      public id: number,
      public primary: Person,
      public secondary?: Person,
      public children?: Person[]) {
        // this.primary = new Person();
        // this.secondary  = new Person();
      }

    //  constructor(
    //   public Participants: Person[]) {}
}
