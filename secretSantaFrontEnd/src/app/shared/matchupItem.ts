import { Person } from './person';

export class MatchupItem {

    constructor(
      public matchPerson1: Person,
      public matchPerson2: Person) {}
}
