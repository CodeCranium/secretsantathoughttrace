import { MatchupItem } from '../shared/matchupItem';
import { ParticipantService } from '../services/participant.service';
import { Person } from '../shared/person';
import { Group } from '../shared/group';
import { Component, NgZone, Input } from '@angular/core';
import { Observable } from 'rxjs';


@Component({
  selector: 'secretsanta-form',
  templateUrl: './secretSantaForm.component.html',
  styleUrls: [ './secretSantaForm.component.css' ]
})
export class SecretSantaFormComponent  {
  fieldArray: Array<Group> = [
    // {
    //   id: 1,
    //   primary: new Person("Bruce", "Bowen"),
    //   secondary: new Person("Angie", "Bowen"),
    //   children: [new Person('Lincoln', 'Bowen')]
    // },
    // {
    //   primary: new Person("Mitch", "Branson"),
    //   secondary: new Person("Janice", "Branson")
    // },
  ];
  newAttribute: Group;
  isEditItems: boolean;

  matchupResults: MatchupItem[];
  resultCount: number;

  constructor(private participantService: ParticipantService, public zone: NgZone) {
    participantService.getParticipants().subscribe((res) => {
      this.fieldArray = res;
    });
    // this.matchupResults = [];
    this.isEditItems = true;
  }

  addFieldValue(index) {
      this.fieldArray.push(this.newAttribute);
      this.newAttribute = new Group(10, new Person(), new Person(), new Person()[2]);
  }

  deleteFieldValue(index) {
    this.fieldArray.splice(index, 1);
  }

  onEditCloseItems() {
    this.isEditItems = !this.isEditItems;
  }

  generateMatchupList() {
    this.participantService.generateMatchupList(this.fieldArray).subscribe((res) => {
      this.zone.run(() => {
        this.matchupResults = res;

        this.resultCount = this.getMatchupResultCount();
      });
    });
  }

  getMatchupResultCount(): number {
    let participantCount = 0;
    this.matchupResults.forEach(element => {
      if (element.matchPerson1 != null) {
        participantCount ++;
      }
      if (element.matchPerson2 != null) {
        participantCount ++;
      }
    });
    return participantCount;
  }

  // onChangeValue(event: any, indexInArray: number) {
  //   alert(event.target.value);
  // }

}
